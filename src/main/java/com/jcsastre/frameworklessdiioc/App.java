package com.jcsastre.frameworklessdiioc;

import com.jcsastre.frameworklessdiioc.service.ServiceA;
import com.jcsastre.frameworklessdiioc.service.ServiceB;
import com.jcsastre.frameworklessdiioc.service.context.AppContexSinglentonImpl;

public class App {

    public static void main( String[] args ) {

//        final AppContextImpl appContext = new AppContextImpl();
        final AppContexSinglentonImpl appContext = AppContexSinglentonImpl.getInstance();

        final ServiceA serviceA = appContext.serviceA();
        final ServiceB serviceB = appContext.serviceB();

        serviceA.doA();
        System.out.println("*********");
        serviceB.doB();
    }
}
