package com.jcsastre.frameworklessdiioc.service.impl;

import com.jcsastre.frameworklessdiioc.service.ServiceA;
import com.jcsastre.frameworklessdiioc.service.ServiceB;

/**
 * @author: Juan Carlos Sastre
 */
public class ServiceBImpl implements ServiceB {

    private ServiceA serviceA;

    public ServiceBImpl(ServiceA serviceA) {
        this.serviceA = serviceA;
    }


    public void doB() {
        serviceA.doA();
        System.out.println("doB");
    }
}
