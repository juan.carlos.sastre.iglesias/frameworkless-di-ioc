package com.jcsastre.frameworklessdiioc.service.impl;

import com.jcsastre.frameworklessdiioc.service.ServiceA;

public class ServiceAImpl implements ServiceA {

    public void doA() {
        System.out.println("doA");
    }
}
