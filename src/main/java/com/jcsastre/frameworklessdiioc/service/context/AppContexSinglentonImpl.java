package com.jcsastre.frameworklessdiioc.service.context;

import com.jcsastre.frameworklessdiioc.service.ServiceA;
import com.jcsastre.frameworklessdiioc.service.ServiceB;
import com.jcsastre.frameworklessdiioc.service.impl.ServiceAImpl;
import com.jcsastre.frameworklessdiioc.service.impl.ServiceBImpl;

public class AppContexSinglentonImpl implements AppContext {

    private static AppContexSinglentonImpl appContexSinglenton;

    private ServiceA serviceA;
    private ServiceB serviceB;

    private AppContexSinglentonImpl() {

        serviceA = new ServiceAImpl();
        serviceB = new ServiceBImpl(serviceA());
    }

    public static AppContexSinglentonImpl getInstance() {

        if (appContexSinglenton == null)
            appContexSinglenton = new AppContexSinglentonImpl();

        return appContexSinglenton;
    }

    public ServiceA serviceA() {
        return serviceA;
    }

    public ServiceB serviceB() {
        return serviceB;
    }
}
