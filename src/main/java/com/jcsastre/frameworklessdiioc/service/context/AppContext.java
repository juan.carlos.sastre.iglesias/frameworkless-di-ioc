package com.jcsastre.frameworklessdiioc.service.context;

import com.jcsastre.frameworklessdiioc.service.ServiceA;
import com.jcsastre.frameworklessdiioc.service.ServiceB;

public interface AppContext {
    ServiceA serviceA();

    ServiceB serviceB();
}
