package com.jcsastre.frameworklessdiioc.service.context;

import com.jcsastre.frameworklessdiioc.service.ServiceA;
import com.jcsastre.frameworklessdiioc.service.ServiceB;
import com.jcsastre.frameworklessdiioc.service.impl.ServiceAImpl;
import com.jcsastre.frameworklessdiioc.service.impl.ServiceBImpl;

public class AppContextImpl implements AppContext {

    private ServiceA serviceA;
    private ServiceB serviceB;

    public synchronized ServiceA serviceA() {

        if (serviceA == null)
            serviceA = new ServiceAImpl();

        return serviceA;
    }

    public synchronized ServiceB serviceB() {

        if (serviceB == null)
            serviceB = new ServiceBImpl(serviceA());

        return serviceB;
    }
}
